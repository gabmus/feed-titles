# Feed Titles

Simple Python script based on [Syndication Domination](https://gitlab.com/gabmus/syndication-domination) to parse feeds, and print their items in chronological order.

Best if used with a pager that supports colors, like `most`.

## Dependencies

- [python-syndom](https://gitlab.com/gabmus/syndication-domination) ([available in the AUR](https://aur.archlinux.org/packages/python-syndom-git))
- python-colorama
- python-requests
- python-dateutil
