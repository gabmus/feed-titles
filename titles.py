#!/usr/bin/env python3

import requests
from syndom import Feed as SDFeed
from dateutil.parser import parse as dateparse
from hashlib import sha256
from os.path import isdir, isfile
from os import makedirs
from sys import argv

feed_urls = list()

no_colors = '--no-colors' in argv
if no_colors:
    argv.remove('--no-colors')

if len(argv) < 2:
    print(f'Usage: {argv[0]} [--no-colors] path_to_file.txt\n')
    print('The file passed should contain a list of Feed urls, one per line')
    print('--no-colors disables the fancy colors')
    exit(0)

# load feed list
with open(argv[1], 'r') as fd:
    feed_urls = [f.strip() for f in fd.readlines()]


# download
def sha(s):
    return sha256(s.encode()).hexdigest()

GET_HEADERS = {
    'User-Agent': 'feed-titles/1.0',
    'Accept': '*/*',
    'Accept-Encoding': 'gzip, deflate'
}

feed_paths = []

if not isdir('cache'):
    makedirs('cache')

for feed_url in feed_urls:
    try:
        res = requests.get(
            feed_url, headers=GET_HEADERS, allow_redirects=True,
            timeout=30
        )
    except requests.exceptions.ConnectTimeout:
        continue
    dest = 'cache/'+sha(feed_url)+'.rss'
    with open(dest, 'wb') as fd:
        fd.write(res.content)
    feed_paths.append(dest)


# parse
class FeedItem:
    def __init__(self, sd_fi, sd_feed):
        self.sd_fi = sd_fi
        self.sd_feed = sd_feed
        self.feed_name = self.sd_feed.title
        self.title = self.sd_fi.title
        self.pub_date = (
            dateparse(self.sd_fi.pub_date) if self.sd_fi.pub_date
            else dateparse(self.sd_feed.last_update)
        )
        self.url = self.sd_fi.url


class Feed:
    def __init__(self, path):
        self.path = path
        self.sd_feed = SDFeed(self.path)
        self.title = self.sd_feed.title
        if self.sd_feed.items[0].pub_date:
            self.items = [
                FeedItem(sd_fi, self.sd_feed) for sd_fi in self.sd_feed.items
            ]
        else:
            self.items = [FeedItem(self.sd_feed.items[0], self.sd_feed)]


feeds = []
items = []
for fp in feed_paths:
    feed = Feed(fp)
    feeds.append(feed)
    items.extend(feed.items)

items.sort(key=lambda fi: fi.pub_date, reverse=True)

# print
if no_colors:
    for item in items:
        print(
            f'[{item.pub_date.date()}]',
            f'{item.feed_name}:\n\t{item.title}\n\t({item.url})\n'
        )
else:
    from colorama import Style
    for item in items:
        print(
            f'{Style.DIM}[{item.pub_date.date()}]{Style.RESET_ALL}',
            f'{Style.BRIGHT}{item.feed_name}{Style.RESET_ALL}:\n\t'
            f'{item.title}\n\t{Style.DIM}({item.url}){Style.RESET_ALL}\n'
        )
